# Projet kitesurf c#

![landscape](res/img/landscape.jpg)

## Introduction

Vous venez de découvrir le kitesurf et voulez profiter de toutes les occasions pour pratiquer dans de bonnes conditions ce sport de glisse dans votre coin préféré.

## Données techniques

### Correspondance poids, aile et vent

Le kitesurf est intimement lié aux conditions météo.

Voici un tableau mettant en relation la taille de l'aile (en mètres carrés) et la plage de vent recommandée (en noeuds). 1 noeud est équivalent à 1.85 km/h.

![wind-wing](res/img/wind-wing.png)

Par exemple avec une aile de kitesurf de 12M2, un vent stable entre 12 et 18 noeuds sera idéal.

Le choix de la taille de l'aile est lié au poids de la personne selon la correspondance suivante:

Vous pesez entre 50 et 60 Kg : 9 m²

Vous pesez entre 60 et 65 Kg : 9 – 10 m²

Vous pesez entre 65 et 70 Kg : 10 – 11 m²

Vous pesez entre 70 et 75 Kg : 12 m²

Vous pesez entre 75 et 80 Kg : 13 m²

Vous pesez entre 80 et 90 Kg : 14 m²

Vous pesez + de 90Kg: 15 m²

Enfin, la direction du vent a beaucoup d'importance. Les vents vous ramenant naturellement vers la plage sont à privilégier ("on-shore"). A l'inverse, les vents vous emmenant au large ("off-shore") sont donc à éviter.

### Données du vent

Habitant à Genève, vous vous intéressez plus particulièrement à la plage du Vengeron qui est une zone réputée pour le kitesurf.

![beach-vengeron](res/img/beach-vengeron.jpg)
[webcam](https://vengeron.roundshot.com)

Le lien suivant fournit au format json toutes les 10mn les informations d'une station météo située à proximité (Cointrin):

[meteo-10mn-cointrin](https://data.geo.admin.ch/ch.meteoschweiz.messwerte-windgeschwindigkeit-kmh-10min/ch.meteoschweiz.messwerte-windgeschwindigkeit-kmh-10min_en.json)

Dans cette zone, on considèrera qu'un vent pousse vers le large ("off-shore") pour les directions de vent situées entre 0 et 180°.

## Choix de conception

Vous décidez de concevoir un outil dans le langage C# récoltant en continu les valeurs de vent et de direction de cette station météo "Genève / Cointrin" et les archivant sous forme de base de données dans votre logiciel.

Une interface graphique sera à disposition de l'utilisateur lui permettant de renseigner son poids. Le logiciel calculera ensuite automatiquement la taille de la voile adaptée puis indiquera à l'utilisateur si le vent lui permet actuellement de pratiquer le kitesurf.

En complément, grâce à l'ensemble de l'historique des données météo récoltées par votre logiciel, vous afficherez sous une forme visuelle (ex: calendrier) sur la dernière année glissante les dates auxquelles l'utilisateur aurait pu faire du kitesurf.

Que ce soit pour la pratique immédiate du kitesurf ou pour l'historique, l'affichage devra tenir compte de la taille de la voile correspondant au poids de l'utilisateur.

L'utilisateur étant supposé débutant, il faudra exclure  toutes les directions de vent vers le large ("off-shore").



# Références sitographiques

[Backside-shop Morges pour la plage des vents](https://www.backside-shop.com/)

